yiddish-keys-windows
====================

Yiddish keyboard layout for Windows

Linux version: https://github.com/heyheydanhey/yiddish-keys-linux

OSX version: https://github.com/heyheydanhey/yiddish-keys-osx

More info: http://www.shretl.org


HEY DUMDUM, HEY SCHMO 
---------------------
**Didn't you know you can write Yiddish with a Hebrew layout?**


Like a frozen forest bare of birds and leaves, Yiddish orthography on the internet 
has shed its defining hats, dots and strikes because of the lack of alternatives 
to Modern Hebrew layouts, for which all such things are superfluous. Not only that, 
but Hebrew's prioritization of different letters to Yiddish means that typing is so
frustrating for many that they prefer typing in Latin characters than Hebrew. 
Transliteration hurts the language - it prevents the development of litteracy, 
especially for those who come to the language as students rather than through birth.

This Yiddish keyboard hopes to address this by providing an intuitive and eventually
standard Yiddish keyboard for all major OSs to ultimately, it is hoped, include 
as vanilla feature.

Two source-files are included here

  * a qwerty based layout
  * an Israeli-hebrew based layout

They need compilation through Microsoft's Keyboard Layout Creator

http://msdn.microsoft.com/en-us/goglobal/bb964665.aspx

Precompiled files: http://www.shretl.org
